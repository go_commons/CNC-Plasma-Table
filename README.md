# Accessible open source CNC Plasma Cutter Kit
This project is not being maintained.

![Name of the machine](/images/render-cnc.2.jpg)

[//]: # (this is a comment)

> #### Designed for local distributed manufacturing

> ##### The current design has been developed, build and tested by @polemidis

# Release notes 🚀
This product is under a final development stage, therefore the documentation provided is still incomplete. The goal is to make it easy for people to get **built this cnc plasma cutter for usage** or **order a kit that can be assembled locally**. The content and ``.md`` files eventually will document all the information to use CAD, CAM files, as well as folder structure and more.


